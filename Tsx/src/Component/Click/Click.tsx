import React, {useState} from 'react';
import './Click.css'
import {type} from "os";

interface Props {
}

const Click : React.FC<Props> =  (props : Props) => {

    const [c911, setC911] = useState<boolean>(false)
    const [c912, setC912] = useState<boolean>(false)
    const [c913, setC913] = useState<boolean>(false)
    const [c921, setC921] = useState<boolean>(false)
    const [c922, setC922] = useState<boolean>(false)
    const [c931, setC931] = useState<boolean>(false)
    const [c932, setC932] = useState<boolean>(false)
    const [c941, setC941] = useState<boolean>(false)
    const [c942, setC942] = useState<boolean>(false)
    const [c943, setC943] = useState<boolean>(false)

    function t911() : void {
        setC911(!c911)
        setC912(false)
        setC913(false)
        setC921(false)
        setC922(false)
        setC931(false)
        setC932(false)
        setC941(false)
        setC942(false)
        setC943(false)
    }

    function t912() : void {
        setC912(!c912)
        setC911(false)
        setC913(false)
        setC921(false)
        setC922(false)
        setC931(false)
        setC932(false)
        setC941(false)
        setC942(false)
        setC943(false)
    }

    function t913() : void {
        setC913(!c913)
        setC912(false)
        setC911(false)
        setC921(false)
        setC922(false)
        setC931(false)
        setC932(false)
        setC941(false)
        setC942(false)
        setC943(false)
    }

    function t921(): void {
        setC921(!c921)
        setC913(false)
        setC912(false)
        setC911(false)
        setC922(false)
        setC931(false)
        setC932(false)
        setC941(false)
        setC942(false)
        setC943(false)
    }

    function t922() : void {
        setC922(!c922)
        setC921(false)
        setC913(false)
        setC912(false)
        setC911(false)
        setC931(false)
        setC932(false)
        setC941(false)
        setC942(false)
        setC943(false)

    }

    function t931() :void {
        setC931(!c931)
        setC922(false)
        setC921(false)
        setC913(false)
        setC912(false)
        setC911(false)
        setC932(false)
        setC941(false)
        setC942(false)
        setC943(false)

    }

    function t932() : void {
        setC932(!c932)
        setC931(false)
        setC922(false)
        setC921(false)
        setC913(false)
        setC912(false)
        setC911(false)
        setC941(false)
        setC942(false)
        setC943(false)

    }

    function t941() :void {
        setC941(!c941)
        setC932(false)
        setC931(false)
        setC922(false)
        setC921(false)
        setC913(false)
        setC912(false)
        setC911(false)
        setC942(false)
        setC943(false)

    }

    function t942() : void {
        setC942(!c942)
        setC941(false)
        setC932(false)
        setC931(false)
        setC922(false)
        setC921(false)
        setC913(false)
        setC912(false)
        setC911(false)
        setC943(false)

    }

    function t943() :void {
        setC943(!c943)
        setC942(false)
        setC941(false)
        setC932(false)
        setC931(false)
        setC922(false)
        setC921(false)
        setC913(false)
        setC912(false)
        setC911(false)

    }

    function t901() :void {

        setC943(false)
        setC942(false)
        setC941(false)
        setC932(false)
        setC931(false)
        setC922(false)
        setC921(false)
        // setC913(false)
        // setC912(false)
        // setC911(false)

    }

    function t902() :void {

        setC943(false)
        setC942(false)
        setC941(false)
        setC932(false)
        setC931(false)
        // setC922(false)
        // setC921(false)
        setC913(false)
        setC912(false)
        setC911(false)

    }

    function t903() :void {

        // setC943(false)
        // setC942(false)
        // setC941(false)
        // setC932(false)
        // setC931(false)
        setC922(false)
        setC921(false)
        setC913(false)
        setC912(false)
        setC911(false)

    }

    function t904() : void {

        // setC943(false)
        // setC942(false)
        // setC941(false)
        setC932(false)
        setC931(false)
        setC922(false)
        setC921(false)
        setC913(false)
        setC912(false)
        setC911(false)

    }

    return (
        <div className={"main9"}>
            <div className={"n90"}>
                <div onClick={t901} className={"n91"}>
                    <div onClick={t911}  className={`${c911  ? "n100" : "n91-1" }`}></div>
                    <div onClick={t912}  className={`${c912  ? "n100" : "n91-1" }`}></div>
                    <div onClick={t913}  className={`${c913  ? "n100" : "n91-1" }`}></div>
                </div>
                <div onClick={t902} className={"n92"}>
                    <div  onClick={t921} className={`${c921  ? "n100" : "n92-1" }`}></div>
                    <div  onClick={t922} className={`${c922  ? "n100" : "n92-1" }`}></div>
                </div>
            </div>
            <div onClick={t903} className={"n93"}>
                <div className={"n95"}>
                    <div  onClick={t931} className={`${c931  ? "n100" : "n93-1" }`}></div>
                    <div  onClick={t932} className={`${c932  ? "n100" : "n93-1" }`}></div>
                </div>
                <div onClick={t904} className={"n94"}>
                    <div onClick={t941} className={`${c941  ? "n101" : "n94-1" }`}></div>
                    <div  onClick={t942} className={`${c942  ? "n101" : "n94-1" }`}></div>
                    <div onClick={t943} className={`${c943  ? "n101" : "n94-1" }`}></div>
                </div>
            </div>
        </div>
    );
}

export default Click;